<?php
/**
 * @file
 * trrybe_content_types_and_terms.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function trrybe_content_types_and_terms_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_file_default_displays_alter().
 */
function trrybe_content_types_and_terms_file_default_displays_alter(&$data) {
  if (isset($data['image__default__file_field_file_default'])) {
    $data['image__default__file_field_file_default']->status = FALSE; /* WAS: TRUE */
  }
  if (isset($data['image__default__file_field_image'])) {
    $data['image__default__file_field_image']->settings['image_style'] = 'panopoly_image_original'; /* WAS: '' */
  }
  if (isset($data['image__preview__file_field_file_default'])) {
    $data['image__preview__file_field_file_default']->status = FALSE; /* WAS: TRUE */
  }
  if (isset($data['image__preview__file_field_image'])) {
    $data['image__preview__file_field_image']->settings['image_style'] = 'panopoly_image_thumbnail'; /* WAS: 'thumbnail' */
  }
  if (isset($data['image__preview__file_field_media_large_icon'])) {
    $data['image__preview__file_field_media_large_icon']->status = FALSE; /* WAS: TRUE */
  }
  if (isset($data['image__teaser__file_field_file_default'])) {
    $data['image__teaser__file_field_file_default']->status = FALSE; /* WAS: TRUE */
  }
  if (isset($data['image__teaser__file_field_image'])) {
    $data['image__teaser__file_field_image']->settings['image_style'] = 'panopoly_image_quarter'; /* WAS: 'medium' */
  }
  if (isset($data['video__default__file_field_file_video'])) {
    $data['video__default__file_field_file_video']->status = FALSE; /* WAS: TRUE */
  }
  if (isset($data['video__default__media_vimeo_video'])) {
    $data['video__default__media_vimeo_video']->settings['api'] = 1; /* WAS: 0 */
    $data['video__default__media_vimeo_video']->settings['byline'] = 0; /* WAS: 1 */
    $data['video__default__media_vimeo_video']->settings['height'] = 495; /* WAS: 360 */
    $data['video__default__media_vimeo_video']->settings['portrait'] = 0; /* WAS: 1 */
    $data['video__default__media_vimeo_video']->settings['protocol'] = '//'; /* WAS: 'https://' */
    $data['video__default__media_vimeo_video']->settings['width'] = 860; /* WAS: 640 */
  }
  if (isset($data['video__default__media_youtube_video'])) {
    $data['video__default__media_youtube_video']->settings['captions'] = FALSE; /* WAS: '' */
    $data['video__default__media_youtube_video']->settings['height'] = 495; /* WAS: 390 */
    $data['video__default__media_youtube_video']->settings['width'] = 860; /* WAS: 640 */
  }
  if (isset($data['video__preview__media_vimeo_image'])) {
    $data['video__preview__media_vimeo_image']->settings['image_style'] = 'popular_homepage_video'; /* WAS: 'media_thumbnail' */
  }
  if (isset($data['video__preview__media_youtube_image'])) {
    $data['video__preview__media_youtube_image']->settings['image_style'] = 'popular_homepage_video'; /* WAS: 'panopoly_image_thumbnail' */
  }
}

/**
 * Implements hook_image_default_styles().
 */
function trrybe_content_types_and_terms_image_default_styles() {
  $styles = array();

  // Exported image style: channel_image_style.
  $styles['channel_image_style'] = array(
    'name' => 'channel_image_style',
    'label' => 'channel_image_style',
    'effects' => array(
      17 => array(
        'label' => 'Manual Crop: Crop and scale',
        'help' => 'Crop and scale a user-selected area, respecting the ratio of the destination width and height.',
        'effect callback' => 'manualcrop_crop_and_scale_effect',
        'form callback' => 'manualcrop_crop_and_scale_form',
        'summary theme' => 'manualcrop_crop_and_scale_summary',
        'module' => 'manualcrop',
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 275,
          'height' => 185,
          'upscale' => 0,
          'respectminimum' => 0,
          'onlyscaleifcrop' => 0,
          'style_name' => 'channel_image_style',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: channels_listing_videos.
  $styles['channels_listing_videos'] = array(
    'name' => 'channels_listing_videos',
    'label' => 'channels listing videos',
    'effects' => array(
      16 => array(
        'label' => 'Manual Crop: Crop and scale',
        'help' => 'Crop and scale a user-selected area, respecting the ratio of the destination width and height.',
        'effect callback' => 'manualcrop_crop_and_scale_effect',
        'form callback' => 'manualcrop_crop_and_scale_form',
        'summary theme' => 'manualcrop_crop_and_scale_summary',
        'module' => 'manualcrop',
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 180,
          'height' => 125,
          'upscale' => 1,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'channels_listing_videos',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: latest_activity_350px__195px.
  $styles['latest_activity_350px__195px'] = array(
    'name' => 'latest_activity_350px__195px',
    'label' => 'Latest activity 350px, 195px',
    'effects' => array(
      15 => array(
        'label' => 'Manual Crop: Crop and scale',
        'help' => 'Crop and scale a user-selected area, respecting the ratio of the destination width and height.',
        'effect callback' => 'manualcrop_crop_and_scale_effect',
        'form callback' => 'manualcrop_crop_and_scale_form',
        'summary theme' => 'manualcrop_crop_and_scale_summary',
        'module' => 'manualcrop',
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 350,
          'height' => 195,
          'upscale' => 1,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'latest_activity_350px__195px',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: popular_homepage_video.
  $styles['popular_homepage_video'] = array(
    'name' => 'popular_homepage_video',
    'label' => 'popular homepage video',
    'effects' => array(
      13 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 765,
          'height' => 460,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: user_logo_image.
  $styles['user_logo_image'] = array(
    'name' => 'user_logo_image',
    'label' => 'user_logo_image',
    'effects' => array(
      20 => array(
        'label' => 'Manual Crop: Crop and scale',
        'help' => 'Crop and scale a user-selected area, respecting the ratio of the destination width and height.',
        'effect callback' => 'manualcrop_crop_and_scale_effect',
        'form callback' => 'manualcrop_crop_and_scale_form',
        'summary theme' => 'manualcrop_crop_and_scale_summary',
        'module' => 'manualcrop',
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 35,
          'height' => 35,
          'upscale' => 0,
          'respectminimum' => 0,
          'onlyscaleifcrop' => 0,
          'style_name' => 'user_logo_image',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: video_homepage.
  $styles['video_homepage'] = array(
    'name' => 'video_homepage',
    'label' => 'video homepage',
    'effects' => array(
      14 => array(
        'label' => 'Manual Crop: Crop and scale',
        'help' => 'Crop and scale a user-selected area, respecting the ratio of the destination width and height.',
        'effect callback' => 'manualcrop_crop_and_scale_effect',
        'form callback' => 'manualcrop_crop_and_scale_form',
        'summary theme' => 'manualcrop_crop_and_scale_summary',
        'module' => 'manualcrop',
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 854,
          'height' => 480,
          'upscale' => 1,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'video_homepage',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: video_thumbnail.
  $styles['video_thumbnail'] = array(
    'name' => 'video_thumbnail',
    'label' => 'video_thumbnail',
    'effects' => array(
      18 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 200,
          'height' => 200,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
      19 => array(
        'label' => 'Resize',
        'help' => 'Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.',
        'effect callback' => 'image_resize_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_resize',
        'data' => array(
          'width' => 170,
          'height' => 100,
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function trrybe_content_types_and_terms_node_info() {
  $items = array(
    'upload_video' => array(
      'name' => t('Upload Video'),
      'base' => 'node_content',
      'description' => t('You can upload video here.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
