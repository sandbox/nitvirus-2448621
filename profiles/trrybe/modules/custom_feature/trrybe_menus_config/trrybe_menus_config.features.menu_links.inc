<?php
/**
 * @file
 * trrybe_menus_config.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function trrybe_menus_config_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_create-channel:admin/structure/taxonomy/channels/add
  $menu_links['main-menu_create-channel:admin/structure/taxonomy/channels/add'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'admin/structure/taxonomy/channels/add',
    'router_path' => 'admin/structure/taxonomy/%/add',
    'link_title' => 'Create Channel',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_create-channel:admin/structure/taxonomy/channels/add',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_create-tag:admin/structure/taxonomy/video_category/add
  $menu_links['main-menu_create-tag:admin/structure/taxonomy/video_category/add'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'admin/structure/taxonomy/video_category/add',
    'router_path' => 'admin/structure/taxonomy/%/add',
    'link_title' => 'CREATE TAG',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_create-tag:admin/structure/taxonomy/video_category/add',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: main-menu_home:<front>
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'HOME',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_my-channels:menutoken/551bfc7443662
  $menu_links['main-menu_my-channels:menutoken/551bfc7443662'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'menutoken/551bfc7443662',
    'router_path' => 'menutoken/%',
    'link_title' => 'My Channels',
    'options' => array(
      'menu_token_link_path' => 'video/channels',
      'menu_token_data' => array(),
      'menu_token_options' => array(
        'clear' => 0,
      ),
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_my-channels:menutoken/551bfc7443662',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => -1,
    'customized' => 1,
    'parent_identifier' => 'main-menu_my-videos:video',
  );
  // Exported menu link: main-menu_my-playlist:menutoken/551bfca97f983
  $menu_links['main-menu_my-playlist:menutoken/551bfca97f983'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'menutoken/551bfca97f983',
    'router_path' => 'menutoken/%',
    'link_title' => 'My Playlist',
    'options' => array(
      'menu_token_link_path' => 'video/playlist',
      'menu_token_data' => array(),
      'menu_token_options' => array(
        'clear' => 0,
      ),
      'attributes' => array(
        'title' => '',
      ),
      'html' => 1,
      'alter' => TRUE,
      'identifier' => 'main-menu_my-playlist:menutoken/551bfca97f983',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => -1,
    'customized' => 1,
    'parent_identifier' => 'main-menu_my-videos:video',
  );
  // Exported menu link: main-menu_my-videos:video
  $menu_links['main-menu_my-videos:video'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'video',
    'router_path' => 'video',
    'link_title' => 'MY VIDEOS',
    'options' => array(
      'attributes' => array(
        'title' => 'This is my video page',
      ),
      'alter' => TRUE,
      'html' => 1,
      'identifier' => 'main-menu_my-videos:video',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_upload:node/add/upload-video
  $menu_links['main-menu_upload:node/add/upload-video'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/add/upload-video',
    'router_path' => 'node/add/upload-video',
    'link_title' => 'UPLOAD',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'html' => 1,
      'alter' => TRUE,
      'identifier' => 'main-menu_upload:node/add/upload-video',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: user-menu_hi-current-username-current-userpicture:http://localhost/stable/users/admin
  $menu_links['user-menu_hi-current-username-current-userpicture:http://localhost/stable/users/admin'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'http://localhost/stable/users/admin',
    'router_path' => '',
    'link_title' => 'Hi, [current-user:name] [current-user:picture]',
    'options' => array(
      'menu_token_link_path' => '[current-user:url]',
      'menu_token_data' => array(),
      'menu_token_options' => array(
        'clear' => 0,
      ),
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'html' => 1,
      'identifier' => 'user-menu_hi-current-username-current-userpicture:http://localhost/stable/users/admin',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: user-menu_log-in:user/login
  $menu_links['user-menu_log-in:user/login'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user/login',
    'router_path' => 'user/login',
    'link_title' => 'Log in',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'user-menu_log-in:user/login',
    ),
    'module' => 'system',
    'hidden' => -1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'user-menu_my-account:user',
  );
  // Exported menu link: user-menu_log-out:user/logout
  $menu_links['user-menu_log-out:user/logout'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user/logout',
    'router_path' => 'user/logout',
    'link_title' => 'Log out',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'user-menu_log-out:user/logout',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 10,
    'customized' => 0,
  );
  // Exported menu link: user-menu_user-account:user
  $menu_links['user-menu_user-account:user'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user',
    'router_path' => 'user',
    'link_title' => 'User account',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'user-menu_user-account:user',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -10,
    'customized' => 0,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('CREATE TAG');
  t('Create Channel');
  t('HOME');
  t('Hi, [current-user:name] [current-user:picture]');
  t('Log in');
  t('Log out');
  t('MY VIDEOS');
  t('My Channels');
  t('My Playlist');
  t('UPLOAD');
  t('User account');


  return $menu_links;
}
