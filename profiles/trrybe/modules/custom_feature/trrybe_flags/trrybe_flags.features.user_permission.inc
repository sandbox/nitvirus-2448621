<?php
/**
 * @file
 * trrybe_flags.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function trrybe_flags_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer flags'.
  $permissions['administer flags'] = array(
    'name' => 'administer flags',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'flag add_to_playlist'.
  $permissions['flag add_to_playlist'] = array(
    'name' => 'flag add_to_playlist',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'editor' => 'editor',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'flag like_the_video'.
  $permissions['flag like_the_video'] = array(
    'name' => 'flag like_the_video',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'editor' => 'editor',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'flag subscribe_unsubscribe'.
  $permissions['flag subscribe_unsubscribe'] = array(
    'name' => 'flag subscribe_unsubscribe',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'editor' => 'editor',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'unflag add_to_playlist'.
  $permissions['unflag add_to_playlist'] = array(
    'name' => 'unflag add_to_playlist',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'editor' => 'editor',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'unflag like_the_video'.
  $permissions['unflag like_the_video'] = array(
    'name' => 'unflag like_the_video',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'editor' => 'editor',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'unflag subscribe_unsubscribe'.
  $permissions['unflag subscribe_unsubscribe'] = array(
    'name' => 'unflag subscribe_unsubscribe',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'editor' => 'editor',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'use flag import'.
  $permissions['use flag import'] = array(
    'name' => 'use flag import',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'flag',
  );

  return $permissions;
}
