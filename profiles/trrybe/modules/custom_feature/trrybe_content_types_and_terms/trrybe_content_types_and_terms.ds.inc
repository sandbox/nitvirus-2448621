<?php
/**
 * @file
 * trrybe_content_types_and_terms.ds.inc
 */

/**
 * Implements hook_ds_view_modes_info().
 */
function trrybe_content_types_and_terms_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'channels_listing_videos';
  $ds_view_mode->label = 'channels_listing_videos';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'file' => 'file',
  );
  $export['channels_listing_videos'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'homepage_popular_765px_x_460px';
  $ds_view_mode->label = 'homepage popular: 765px X 460px';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'file' => 'file',
  );
  $export['homepage_popular_765px_x_460px'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'latest_activity';
  $ds_view_mode->label = 'Latest activity';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'file' => 'file',
  );
  $export['latest_activity'] = $ds_view_mode;

  return $export;
}
