<?php
/**
 * @file
 * trrybe_playlists_and_channels.features.content_menu_links.inc
 */

/**
 * Implements hook_content_menu_links_defaults().
 */
function trrybe_playlists_and_channels_content_menu_links_defaults() {
  $menu_links = array();

  // Exported menu link: main-menu:video
  $menu_links['main-menu:video'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'video',
    'router_path' => 'video',
    'link_title' => 'MY VIDEOS',
    'options' => array(
      'attributes' => array(
        'title' => 'This is my video page',
      ),
      'alter' => TRUE,
      'html' => 1,
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -49,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('MY VIDEOS');


  return $menu_links;
}
