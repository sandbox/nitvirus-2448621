<?php
/**
 * @file
 * trrybe_menus_config.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function trrybe_menus_config_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_expanded';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_expanded'] = $strongarm;

  return $export;
}
