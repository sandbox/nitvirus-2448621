<?php

/**
 * @file
 * Provide profile configuration.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function trrybe_form_install_configure_form_alter(&$form, $form_state) {

  $form['admin_account']['account']['name']['#default_value'] = 'admin';

  $form['update_notifications']['update_status_module']['#default_value'] = array(1 => FALSE, 2 => FALSE);

  // Set a default country so we can benefit from it on Address Fields.
  $form['server_settings']['site_default_country']['#default_value'] = 'INDIA';

  // Use "admin" as the default username.
  $form['admin_account']['account']['name']['#default_value'] = 'admin';

  $form['admin_account']['account']['trrybe_informations'] = array(
    '#markup' => '<p>' . t('This information will be emailed to the distribution email address.') . '</p>'
  );

}

/**
 * Implements hook_install_tasks_alter().
 */
function trrybe_install_tasks_alter(&$tasks, $install_state) {
  // Magically go one level deeper in solving years of dependency problems.
  require_once(drupal_get_path('module', 'panopoly_core') . '/panopoly_core.profile.inc');
  $tasks['install_load_profile']['function'] = 'panopoly_core_install_load_profile';
  // If we only offer one language, define a callback to set this.
  require_once(drupal_get_path('module', 'panopoly_core') . '/panopoly_core.profile.inc');
  if (!(count(install_find_locales($install_state['parameters']['profile'])) > 1)) {
    $tasks['install_select_locale']['function'] = 'panopoly_core_install_locale_selection';
  }
}


/**
 * Implements hook_install_tasks().
 */
function trrybe_install_tasks($install_state) {
  $tasks = array();
  // Add our custom CSS file for the installation process.
  drupal_add_css(drupal_get_path('profile', 'trrybe') . '/trrybe.css');
}
