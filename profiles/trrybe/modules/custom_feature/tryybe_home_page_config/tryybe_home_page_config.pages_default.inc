<?php
/**
 * @file
 * tryybe_home_page_config.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function tryybe_home_page_config_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'tryybe_homepage';
  $page->task = 'page';
  $page->admin_title = 'homepage';
  $page->admin_description = '';
  $page->path = 'tryybe_homepage';
  $page->access = array(
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'none',
    'title' => '',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_tryybe_homepage__panelizer_node_38741717-a841-4140-bb78-39f47bceb1c5';
  $handler->task = 'page';
  $handler->subtask = 'tryybe_homepage';
  $handler->handler = 'panelizer_node';
  $handler->weight = 2;
  $handler->conf = array(
    'title' => 'Panelizer',
    'contexts' => array(),
    'relationships' => array(),
    'context' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'front',
          'settings' => NULL,
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $page->default_handlers[$handler->name] = $handler;
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_tryybe_homepage__panel_context_471c5d98-95fe-4f03-8f23-78df8b66f7db';
  $handler->task = 'page';
  $handler->subtask = 'tryybe_homepage';
  $handler->handler = 'panel_context';
  $handler->weight = 3;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'radix_boxton';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '6879e5f9-90cf-4cfa-8b99-92757f8eca99';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-277c2518-0b7d-4560-aa4f-473fe1c5bdfe';
    $pane->panel = 'contentmain';
    $pane->type = 'views_panes';
    $pane->subtype = 'popular_videos-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_settings' => 'fields',
      'header_type' => 'none',
      'view_mode' => 'teaser',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'popular-video',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '277c2518-0b7d-4560-aa4f-473fe1c5bdfe';
    $display->content['new-277c2518-0b7d-4560-aa4f-473fe1c5bdfe'] = $pane;
    $display->panels['contentmain'][0] = 'new-277c2518-0b7d-4560-aa4f-473fe1c5bdfe';
    $pane = new stdClass();
    $pane->pid = 'new-057bbf38-6a01-4452-8452-7838aa186701';
    $pane->panel = 'contentmain';
    $pane->type = 'views_panes';
    $pane->subtype = 'newcomments-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_settings' => 'fields',
      'header_type' => 'none',
      'view_mode' => 'teaser',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'latest-activity',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '057bbf38-6a01-4452-8452-7838aa186701';
    $display->content['new-057bbf38-6a01-4452-8452-7838aa186701'] = $pane;
    $display->panels['contentmain'][1] = 'new-057bbf38-6a01-4452-8452-7838aa186701';
    $pane = new stdClass();
    $pane->pid = 'new-d6380479-356e-4249-b1f0-03fbe6e1b6e6';
    $pane->panel = 'contentmain';
    $pane->type = 'views_panes';
    $pane->subtype = 'channels_video_listing-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_settings' => 'fields',
      'header_type' => 'none',
      'view_mode' => 'featured',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'channel-video-listing',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'd6380479-356e-4249-b1f0-03fbe6e1b6e6';
    $display->content['new-d6380479-356e-4249-b1f0-03fbe6e1b6e6'] = $pane;
    $display->panels['contentmain'][2] = 'new-d6380479-356e-4249-b1f0-03fbe6e1b6e6';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-277c2518-0b7d-4560-aa4f-473fe1c5bdfe';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['tryybe_homepage'] = $page;

  return $pages;

}
