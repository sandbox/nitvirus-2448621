<?php
/**
 * @file
 * tryybe_home_page_config.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function tryybe_home_page_config_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_frontpage';
  $strongarm->value = 'tryybe_homepage';
  $export['site_frontpage'] = $strongarm;

  return $export;
}
