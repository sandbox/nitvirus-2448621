api = 2
core = 7.x

; Build Kit ===================================================================

includes[] = http://drupalcode.org/project/buildkit.git/blob_plain/refs/heads/7.x-2.x:/drupal-org.make

; Features ====================================================================

projects[popular_news][type] = module
projects[popular_news][subdir] = features
projects[popular_news][download][type] = "git"
projects[popular_news][download][url] = git://github.com/drupaltraining/feature_core.git

projects[srijan_distribution_content_type][type] = module
projects[srijan_distribution_content_type][subdir] = features
projects[srijan_distribution_content_type][download][type] = "git"
projects[srijan_distribution_content_type][download][url] = git://github.com/drupaltraining/feature_news.git


; Themes ======================================================================

projects[danland][type] = theme
projects[danland][download][type] = git
projects[danland][download][url] = git://github.com/drupaltraining/twist.git

projects[sparkle][type] = theme
projects[sparkle][download][type] = "git"
projects[sparkle][download][url] = git://github.com/drupaltraining/sparkle.git


; ************************************************
; ******************* PANOPOLY *******************

; Note that makefiles are parsed bottom-up, and that in Drush 5 concurrency might
; interfere with recursion.
; Therefore PANOPOLY needs to be listed AT THE BOTTOM of this makefile,
; so we can patch or update certain projects fetched by Panopoly's makefiles.
; NOTE: If you are running Drush 6, this section should be places at the TOP

; Someday maybe we can turn this on to just inherit Panopoly
;projects[panopoly][type] = profile
;projects[panopoly][version] = 1.10
; but, Drupal.org does not support recursive profiles
; and also does not support include[]
; so we need to copy the panopoly.make file here

projects[panopoly_core][version] = 1.16
projects[panopoly_core][subdir] = panopoly

projects[panopoly_images][version] = 1.16
projects[panopoly_images][subdir] = panopoly

projects[panopoly_theme][version] = 1.16
projects[panopoly_theme][subdir] = panopoly

projects[panopoly_magic][version] = 1.16
projects[panopoly_magic][subdir] = panopoly

projects[panopoly_widgets][version] = 1.16
projects[panopoly_widgets][subdir] = panopoly

projects[panopoly_admin][version] = 1.16
projects[panopoly_admin][subdir] = panopoly

projects[panopoly_users][version] = 1.16
projects[panopoly_users][subdir] = panopoly

projects[panopoly_pages][version] = 1.16
projects[panopoly_pages][subdir] = panopoly

projects[panopoly_wysiwyg][version] = 1.16
projects[panopoly_wysiwyg][subdir] = panopoly

projects[panopoly_search][version] = 1.16
projects[panopoly_search][subdir] = panopoly

; ***************** End Panopoly *****************
; ************************************************
