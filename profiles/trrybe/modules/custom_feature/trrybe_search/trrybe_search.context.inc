<?php
/**
 * @file
 * trrybe_search.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function trrybe_search_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'trrybe_search_configuration';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views--exp-trrybe_search_page-page' => array(
          'module' => 'views',
          'delta' => '-exp-trrybe_search_page-page',
          'region' => 'nav_header',
          'weight' => '-10',
        ),
        'block-1' => array(
          'module' => 'block',
          'delta' => '1',
          'region' => 'footer',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['trrybe_search_configuration'] = $context;

  return $export;
}
