<?php
/**
 * @file
 * trrybe_video_detail_page.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function trrybe_video_detail_page_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ajax_comments_notify';
  $strongarm->value = 1;
  $export['ajax_comments_notify'] = $strongarm;

  return $export;
}
