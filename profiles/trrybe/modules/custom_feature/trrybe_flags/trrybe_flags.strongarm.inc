<?php
/**
 * @file
 * trrybe_flags.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function trrybe_flags_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'flag_default_flag_status';
  $strongarm->value = array(
    'like_the_video' => TRUE,
    'add_to_playlist' => TRUE,
  );
  $export['flag_default_flag_status'] = $strongarm;

  return $export;
}
