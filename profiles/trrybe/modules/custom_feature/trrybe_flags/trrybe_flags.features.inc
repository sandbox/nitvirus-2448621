<?php
/**
 * @file
 * trrybe_flags.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function trrybe_flags_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_flag_default_flags().
 */
function trrybe_flags_flag_default_flags() {
  $flags = array();
  // Exported flag: "Add To Playlist".
  $flags['add_to_playlist'] = array(
    'entity_type' => 'node',
    'title' => 'Add To Playlist',
    'global' => 0,
    'types' => array(
      0 => 'upload_video',
    ),
    'flag_short' => 'Add To Playlist',
    'flag_long' => 'Add This Video To Your Playlist',
    'flag_message' => '',
    'unflag_short' => 'Remove From Playlist',
    'unflag_long' => 'Remove This Video To Your Playlist',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'featured' => 'featured',
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'token' => 0,
      'channels_listing_videos' => 0,
      'homepage_popular_765px_x_460px' => 0,
      'latest_activity' => 0,
      'revision' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'i18n' => 0,
    'module' => 'trrybe_flags',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  // Exported flag: "Like the video".
  $flags['like_the_video'] = array(
    'entity_type' => 'node',
    'title' => 'Like the video',
    'global' => 0,
    'types' => array(
      0 => 'upload_video',
    ),
    'flag_short' => 'Like',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Unlike',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'featured' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'i18n' => 0,
    'module' => 'trrybe_flags',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  // Exported flag: "Subscribe and unsubscribe".
  $flags['subscribe_unsubscribe'] = array(
    'entity_type' => 'taxonomy_term',
    'title' => 'Subscribe and unsubscribe',
    'global' => 0,
    'types' => array(
      0 => 'channels',
    ),
    'flag_short' => 'Subscribe',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Unsubscribe',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'featured' => 'featured',
      'token' => 'token',
    ),
    'show_as_field' => 1,
    'show_on_form' => 1,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'module' => 'trrybe_flags',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}
