<?php
/**
 * @file
 * trrybe_playlists_and_channels.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function trrybe_playlists_and_channels_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'trrybe_grid_configuration';
  $context->description = 'This Context provide Trrybe grid configuration';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'video' => 'video',
        'video/channels' => 'video/channels',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'quicktabs-grid_list' => array(
          'module' => 'quicktabs',
          'delta' => 'grid_list',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('This Context provide Trrybe grid configuration');
  $export['trrybe_grid_configuration'] = $context;

  return $export;
}
