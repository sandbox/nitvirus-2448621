<?php
/**
 * @file
 * trrybe_browse_channels.features.inc
 */

/**
 * Implements hook_views_api().
 */
function trrybe_browse_channels_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
