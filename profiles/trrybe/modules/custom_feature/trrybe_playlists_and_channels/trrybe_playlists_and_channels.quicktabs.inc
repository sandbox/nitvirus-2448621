<?php
/**
 * @file
 * trrybe_playlists_and_channels.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function trrybe_playlists_and_channels_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'grid_list';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Grid/list';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'subscribed_channel',
      'display' => 'page',
      'args' => '',
      'title' => 'List',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'subscribed_channel',
      'display' => 'page_1',
      'args' => '',
      'title' => 'Grid',
      'weight' => '-99',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'ui_tabs';
  $quicktabs->style = 'default';
  $quicktabs->options = array(
    'history' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Grid');
  t('Grid/list');
  t('List');

  $export['grid_list'] = $quicktabs;

  return $export;
}
