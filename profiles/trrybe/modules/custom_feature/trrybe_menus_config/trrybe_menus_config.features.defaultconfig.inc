<?php
/**
 * @file
 * trrybe_menus_config.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function trrybe_menus_config_defaultconfig_features() {
  return array(
    'trrybe_menus_config' => array(
      'strongarm' => 'strongarm',
    ),
  );
}

/**
 * Implements hook_defaultconfig_strongarm().
 */
function trrybe_menus_config_defaultconfig_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_expanded';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_expanded'] = $strongarm;

  return $export;
}
