/**
* Home page available channel toggle scripts *
**/

 $(function() {
    var isMobile = {
Android: function() { return navigator.userAgent.match(/Android/i); },
BlackBerry: function() { return navigator.userAgent.match(/BlackBerry/i); },
iOS: function() { return navigator.userAgent.match(/iPhone/i); },
Opera: function() { return navigator.userAgent.match(/Opera Mini/i); },
Windows: function() { return navigator.userAgent.match(/IEMobile/i); },
any: function() { return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows()); } };
  if (isMobile.any()) {
    $('div.views-limit-grouping-group h3').siblings().hide();
    $('div.views-limit-grouping-group h3').each(function(){
      var sib = $(this).siblings();
      $(this).find('div.taxonomy_name').append('<div class="home_toggle"></div>');
      $(this).find('div.taxonomy_name').click(function(e){
        e.preventDefault();
        sib.toggle("slow");
      });
    });
  }

  });
