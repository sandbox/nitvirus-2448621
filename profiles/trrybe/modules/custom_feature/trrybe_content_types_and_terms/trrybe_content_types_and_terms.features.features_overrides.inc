<?php
/**
 * @file
 * trrybe_content_types_and_terms.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function trrybe_content_types_and_terms_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: file_display
  $overrides["file_display.image__default__file_field_file_default.status"] = FALSE;
  $overrides["file_display.image__default__file_field_image.settings|image_style"] = 'panopoly_image_original';
  $overrides["file_display.image__preview__file_field_file_default.status"] = FALSE;
  $overrides["file_display.image__preview__file_field_image.settings|image_style"] = 'panopoly_image_thumbnail';
  $overrides["file_display.image__preview__file_field_media_large_icon.status"] = FALSE;
  $overrides["file_display.image__teaser__file_field_file_default.status"] = FALSE;
  $overrides["file_display.image__teaser__file_field_image.settings|image_style"] = 'panopoly_image_quarter';
  $overrides["file_display.video__default__file_field_file_video.status"] = FALSE;
  $overrides["file_display.video__default__media_vimeo_video.settings|api"] = 1;
  $overrides["file_display.video__default__media_vimeo_video.settings|byline"] = 0;
  $overrides["file_display.video__default__media_vimeo_video.settings|height"] = 495;
  $overrides["file_display.video__default__media_vimeo_video.settings|portrait"] = 0;
  $overrides["file_display.video__default__media_vimeo_video.settings|protocol"] = '//';
  $overrides["file_display.video__default__media_vimeo_video.settings|width"] = 860;
  $overrides["file_display.video__default__media_youtube_video.settings|captions"] = FALSE;
  $overrides["file_display.video__default__media_youtube_video.settings|height"] = 495;
  $overrides["file_display.video__default__media_youtube_video.settings|width"] = 860;
  $overrides["file_display.video__preview__media_vimeo_image.settings|image_style"] = 'popular_homepage_video';
  $overrides["file_display.video__preview__media_youtube_image.settings|image_style"] = 'popular_homepage_video';

 return $overrides;
}
