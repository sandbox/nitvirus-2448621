<?php
/**
 * @file
 * trrybe_content_types_and_terms.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function trrybe_content_types_and_terms_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:upload_video:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'upload_video';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = 'video-wrap';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'node_type',
        'settings' => array(
          'type' => array(
            'upload_video' => 'upload_video',
          ),
        ),
        'context' => 'panelizer',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = 'video-wrap';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'radix_brenham_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'contentmain' => NULL,
      'default' => NULL,
      'header' => NULL,
      'sidebar' => NULL,
    ),
    'contentmain' => array(
      'style' => 'tabs',
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'db8bfec0-4859-4b78-a284-b89fb12bc876';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-9bc9e1c7-1e57-4d9b-8b08-6ae812e334d0';
    $pane->panel = 'contentmain';
    $pane->type = 'views_panes';
    $pane->subtype = 'video_details-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_settings' => 'fields',
      'header_type' => 'none',
      'view_mode' => 'teaser',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9bc9e1c7-1e57-4d9b-8b08-6ae812e334d0';
    $display->content['new-9bc9e1c7-1e57-4d9b-8b08-6ae812e334d0'] = $pane;
    $display->panels['contentmain'][0] = 'new-9bc9e1c7-1e57-4d9b-8b08-6ae812e334d0';
    $pane = new stdClass();
    $pane->pid = 'new-21507c79-8660-4f03-9623-54007d9e677f';
    $pane->panel = 'contentmain';
    $pane->type = 'views_panes';
    $pane->subtype = 'ajax_comments_video_detail-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_settings' => 'fields',
      'header_type' => 'none',
      'view_mode' => 'teaser',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '21507c79-8660-4f03-9623-54007d9e677f';
    $display->content['new-21507c79-8660-4f03-9623-54007d9e677f'] = $pane;
    $display->panels['contentmain'][1] = 'new-21507c79-8660-4f03-9623-54007d9e677f';
    $pane = new stdClass();
    $pane->pid = 'new-33947fb6-6d5a-43cd-8f75-83a3a89a2c65';
    $pane->panel = 'header';
    $pane->type = 'views_panes';
    $pane->subtype = 'video_details-panel_pane_2';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_settings' => 'fields',
      'header_type' => 'none',
      'view_mode' => 'teaser',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '33947fb6-6d5a-43cd-8f75-83a3a89a2c65';
    $display->content['new-33947fb6-6d5a-43cd-8f75-83a3a89a2c65'] = $pane;
    $display->panels['header'][0] = 'new-33947fb6-6d5a-43cd-8f75-83a3a89a2c65';
    $pane = new stdClass();
    $pane->pid = 'new-94dd9985-f6c7-4eec-9cf9-cb91656a6791';
    $pane->panel = 'header';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_add_new_video';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'file_rendered',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'file_view_mode' => 'default',
        'preset' => 'upload_video_image',
        'jwplayer_preset' => 'uploaded_video',
      ),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '94dd9985-f6c7-4eec-9cf9-cb91656a6791';
    $display->content['new-94dd9985-f6c7-4eec-9cf9-cb91656a6791'] = $pane;
    $display->panels['header'][1] = 'new-94dd9985-f6c7-4eec-9cf9-cb91656a6791';
    $pane = new stdClass();
    $pane->pid = 'new-590488ef-6135-4c4f-ad36-37285ece78d1';
    $pane->panel = 'header';
    $pane->type = 'views_panes';
    $pane->subtype = 'node_statistics-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_settings' => 'fields',
      'header_type' => 'none',
      'view_mode' => 'teaser',
    );
    $pane->cache = array();
    $pane->style = array(
      'style' => 'wrapper_raw',
      'settings' => array(
        'title' => array(
          'prefix' => '',
          'suffix' => '',
        ),
        'content' => array(
          'prefix' => '<div class="video-statistics"><div class="video-statistics">',
          'suffix' => '',
        ),
        'theme' => 0,
      ),
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '590488ef-6135-4c4f-ad36-37285ece78d1';
    $display->content['new-590488ef-6135-4c4f-ad36-37285ece78d1'] = $pane;
    $display->panels['header'][2] = 'new-590488ef-6135-4c4f-ad36-37285ece78d1';
    $pane = new stdClass();
    $pane->pid = 'new-1b2997bf-5c90-43b6-8539-57c047085e7f';
    $pane->panel = 'header';
    $pane->type = 'flag_link';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'flag_name' => 'add_to_playlist',
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'title' => array(
          'prefix' => '',
          'suffix' => '',
        ),
        'content' => array(
          'prefix' => '',
          'suffix' => '</div>',
        ),
        'theme' => 0,
      ),
      'style' => 'wrapper_raw',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '1b2997bf-5c90-43b6-8539-57c047085e7f';
    $display->content['new-1b2997bf-5c90-43b6-8539-57c047085e7f'] = $pane;
    $display->panels['header'][3] = 'new-1b2997bf-5c90-43b6-8539-57c047085e7f';
    $pane = new stdClass();
    $pane->pid = 'new-bb6ea689-0e91-40db-a9b0-b0c31a8900e5';
    $pane->panel = 'header';
    $pane->type = 'flag_link';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'flag_name' => 'like_the_video',
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'style' => 'wrapper_raw',
      'settings' => array(
        'title' => array(
          'prefix' => '',
          'suffix' => '',
        ),
        'content' => array(
          'prefix' => '',
          'suffix' => '</div>',
        ),
        'theme' => 0,
      ),
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = 'bb6ea689-0e91-40db-a9b0-b0c31a8900e5';
    $display->content['new-bb6ea689-0e91-40db-a9b0-b0c31a8900e5'] = $pane;
    $display->panels['header'][4] = 'new-bb6ea689-0e91-40db-a9b0-b0c31a8900e5';
    $pane = new stdClass();
    $pane->pid = 'new-e3477242-8cdc-41a3-8f4e-24238718b893';
    $pane->panel = 'sidebar';
    $pane->type = 'views_panes';
    $pane->subtype = 'video_detail_page-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_settings' => 'fields',
      'header_type' => 'none',
      'view_mode' => 'teaser',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'related-video',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e3477242-8cdc-41a3-8f4e-24238718b893';
    $display->content['new-e3477242-8cdc-41a3-8f4e-24238718b893'] = $pane;
    $display->panels['sidebar'][0] = 'new-e3477242-8cdc-41a3-8f4e-24238718b893';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:upload_video:default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:upload_video:default:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'upload_video';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'default';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'radix_brenham_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'contentmain' => array(),
      'default' => array(),
      'header' => array(),
      'sidebar' => array(),
    ),
    'contentmain' => array(
      'style' => 'stylizer',
    ),
    'style' => 'stylizer',
    'header' => array(
      'style' => 'stylizer',
    ),
    'sidebar' => array(
      'style' => 'stylizer',
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '01617288-fe61-46ae-bda3-237fa472fbc7';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-98e691dc-5f11-4832-9d66-78cf2692e6b6';
    $pane->panel = 'contentmain';
    $pane->type = 'views_panes';
    $pane->subtype = 'video_details-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_settings' => 'fields',
      'header_type' => 'none',
      'view_mode' => 'teaser',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(),
      'style' => 'stylizer',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '98e691dc-5f11-4832-9d66-78cf2692e6b6';
    $display->content['new-98e691dc-5f11-4832-9d66-78cf2692e6b6'] = $pane;
    $display->panels['contentmain'][0] = 'new-98e691dc-5f11-4832-9d66-78cf2692e6b6';
    $pane = new stdClass();
    $pane->pid = 'new-0e8f7964-b236-497a-bd69-30df7d0f791f';
    $pane->panel = 'contentmain';
    $pane->type = 'views_panes';
    $pane->subtype = 'video_comments-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_settings' => 'fields',
      'header_type' => 'none',
      'view_mode' => 'full',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(),
      'style' => 'stylizer',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '0e8f7964-b236-497a-bd69-30df7d0f791f';
    $display->content['new-0e8f7964-b236-497a-bd69-30df7d0f791f'] = $pane;
    $display->panels['contentmain'][1] = 'new-0e8f7964-b236-497a-bd69-30df7d0f791f';
    $pane = new stdClass();
    $pane->pid = 'new-f93bc60f-4d20-4a90-9603-608751752407';
    $pane->panel = 'contentmain';
    $pane->type = 'node_comment_form';
    $pane->subtype = 'node_comment_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'anon_links' => 0,
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(),
      'style' => 'stylizer',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'f93bc60f-4d20-4a90-9603-608751752407';
    $display->content['new-f93bc60f-4d20-4a90-9603-608751752407'] = $pane;
    $display->panels['contentmain'][2] = 'new-f93bc60f-4d20-4a90-9603-608751752407';
    $pane = new stdClass();
    $pane->pid = 'new-a2921324-aadd-49c0-b61a-cba43beb57fa';
    $pane->panel = 'header';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_add_new_video';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'file_rendered',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'file_view_mode' => 'default',
        'jwplayer_preset' => 'uploaded_video',
      ),
      'context' => array(),
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(),
      'style' => 'stylizer',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a2921324-aadd-49c0-b61a-cba43beb57fa';
    $display->content['new-a2921324-aadd-49c0-b61a-cba43beb57fa'] = $pane;
    $display->panels['header'][0] = 'new-a2921324-aadd-49c0-b61a-cba43beb57fa';
    $pane = new stdClass();
    $pane->pid = 'new-2710566f-2aaa-4662-883a-b1270d4c404e';
    $pane->panel = 'header';
    $pane->type = 'views_panes';
    $pane->subtype = 'node_statistics-panel_pane_3';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_settings' => 'fields',
      'header_type' => 'none',
      'view_mode' => 'teaser',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(),
      'style' => 'stylizer',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '2710566f-2aaa-4662-883a-b1270d4c404e';
    $display->content['new-2710566f-2aaa-4662-883a-b1270d4c404e'] = $pane;
    $display->panels['header'][1] = 'new-2710566f-2aaa-4662-883a-b1270d4c404e';
    $pane = new stdClass();
    $pane->pid = 'new-d8272981-2ec7-4ab9-a6d4-195d130f43b6';
    $pane->panel = 'header';
    $pane->type = 'views_panes';
    $pane->subtype = 'node_statistics-panel_pane_4';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_settings' => 'fields',
      'header_type' => 'none',
      'view_mode' => 'teaser',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(),
      'style' => 'stylizer',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'd8272981-2ec7-4ab9-a6d4-195d130f43b6';
    $display->content['new-d8272981-2ec7-4ab9-a6d4-195d130f43b6'] = $pane;
    $display->panels['header'][2] = 'new-d8272981-2ec7-4ab9-a6d4-195d130f43b6';
    $pane = new stdClass();
    $pane->pid = 'new-44082636-bd47-4990-a9e4-f022b7b6d380';
    $pane->panel = 'header';
    $pane->type = 'views_panes';
    $pane->subtype = 'node_statistics-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_settings' => 'fields',
      'header_type' => 'none',
      'view_mode' => 'teaser',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(),
      'style' => 'stylizer',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '44082636-bd47-4990-a9e4-f022b7b6d380';
    $display->content['new-44082636-bd47-4990-a9e4-f022b7b6d380'] = $pane;
    $display->panels['header'][3] = 'new-44082636-bd47-4990-a9e4-f022b7b6d380';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:upload_video:default:default'] = $panelizer;

  return $export;
}
